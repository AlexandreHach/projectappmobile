package com.example.cerimuseum;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.net.URL;
import java.util.ArrayList;
/**************************************************
 * HACHETTE ALEXANDRE
 * 16/04/2020
 * Class where the asyn task is lauch and where we have the list of object of the museum update the information on the main activity
 **************************************************/
public class Museum {

    private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();

    ArrayList<Object> objects;
    MainActivity context;

    Museum(MainActivity context)
    {
        this.context = context;
        objects = new ArrayList<>();
        getMuseumObject();
    }

    public void getMuseumObject() {
        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute();
    }

    private class AsyncTaskRunner extends AsyncTask<URL, String, String> {

        private String resp;

        @Override
        protected String doInBackground(URL... params) {
            IDRecover idRecover = new IDRecover();
            idRecover.recover(objects);
            for(int i = 0; i < objects.size();i++)
            {
                Log.d(TAG, "doInBackground: "+ objects.get(i).getName());
            }
            return "finish";
        }

        @Override
        protected void onPostExecute(String s) {
            Intent intent = new Intent();
            intent.putExtra("objects", objects);
            context.update();
        }

    }



    public ArrayList<Object> getObjectList() {
        return objects;
    }
}
