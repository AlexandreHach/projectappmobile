package com.example.cerimuseum;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
/**************************************************
 * HACHETTE Alexandre
 * 15/04/2020
 * Museum data base
 **************************************************/
public class MuseumDbHelper extends SQLiteOpenHelper {

        private static final String TAG = MuseumDbHelper.class.getSimpleName();

        private static final int DATABASE_VERSION = 1;

        public static final String DATABASE_NAME = "museum.db";

        public static final String TABLE_NAME = "museum";

        public static final String _ID = "_id";
        public static final String COLUMN_OBJECT_NAME = "name";
        public static final String COLUMN_OBJECT_ID = "idObject";
        public static final String COLUMN_OBJECT_CATEGORIE = "categorie";
        public static final String COLUMN_OBJECT_DESCRIPTION = "description";
        public static final String COLUMN_OBJECT_TIMEFRAME = "timeFrame";
        public static final String COLUMN_OBJECT_YEAR = "year";
        public static final String COLUMN_OBJECT_BRAND = "brand";
        public static final String COLUMN_OBJECT_TECHNICALDETAILS = "technicalDetails";
        public static final String COLUMN_OBJECT_WORKING = "working";
        public static final String COLUMN_OBJECT_PICTURES = "Pictures";

        public MuseumDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {

            final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_OBJECT_NAME + " TEXT," +
                    COLUMN_OBJECT_ID + " INTEGER, " +
                    COLUMN_OBJECT_CATEGORIE + "TEXT," +
                    COLUMN_OBJECT_DESCRIPTION + " TEXT, " +
                    COLUMN_OBJECT_TIMEFRAME + " INTEGER, " +
                    COLUMN_OBJECT_YEAR + " TEXT, " +
                    COLUMN_OBJECT_BRAND + " TEXT, " +
                    COLUMN_OBJECT_TECHNICALDETAILS + " TEXT, " +
                    COLUMN_OBJECT_WORKING + " INTEGER, " +
                    COLUMN_OBJECT_PICTURES + " TEXT, " +
                    /** To be sure that we have only one object with this name in the table*/
                    " UNIQUE (" + COLUMN_OBJECT_NAME + ") ON CONFLICT ROLLBACK);";
            db.execSQL(SQL_CREATE_BOOK_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        /**************************************************
         * return ContentValues with all the value of an object
         **************************************************/
        private ContentValues fill(Object object) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_OBJECT_NAME, object.getName());
            values.put(COLUMN_OBJECT_ID, object.getId());
            values.put(COLUMN_OBJECT_CATEGORIE, object.getCategorie());
            values.put(COLUMN_OBJECT_DESCRIPTION, object.getDescription());
            values.put(COLUMN_OBJECT_TIMEFRAME, object.getTimeFrame());
            values.put(COLUMN_OBJECT_YEAR, object.getYear());
            values.put(COLUMN_OBJECT_BRAND, object.getBrand());
            values.put(COLUMN_OBJECT_TECHNICALDETAILS, object.getTechnicalDetails());
            values.put(COLUMN_OBJECT_WORKING, object.isWorking());
            values.put(COLUMN_OBJECT_PICTURES, object.getPictures());
            return values;
        }

        /**************************************************
         * add a new Object to the data base.
         * @return true if object was added to the table false otherwise
         **************************************************/
        public boolean addObject(Object object) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = fill(object);

            Log.d(TAG, "adding: "+object.getName()+" with id="+object.getId());
            //If rowID = -1, an error occured
            long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
            db.close();
            return (rowID != -1);
        }

        /**************************************************
         * Update the information of an object inside the data base
         * @return the number of updated rows
         **************************************************/
        public int updateObject(Object object) {
            /*SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = fill(object);
            Log.d("updateTeam", String.valueOf(object));
            return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                    new String[] { String.valueOf(object.getId()) }, CONFLICT_IGNORE);*/
            return 0;
        }

        /**************************************************
         * @return a cursor on all the object of the data base
         **************************************************/
        public Cursor fetchAllObject() {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.query(TABLE_NAME, null,
                    null, null, null, null, COLUMN_OBJECT_NAME +" ASC", null);
            Log.d(TAG, "call fetchAllTeams()");
            if (cursor != null) {
                cursor.moveToFirst();
            }
            return cursor;
        }

        /**************************************************
         * @return a list on all the teams of the data base
         **************************************************/
        public List<Object> getAllObject() {
            String selectQuery = "SELECT  * FROM " + TABLE_NAME;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            List<Object> res = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    res.add(cursorToObject(cursor));
                } while (cursor.moveToNext());
            }

            return res;
        }
        /** TODO add this functionality if i got time left
        public void deleteObject(int id) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_NAME, _ID + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }*/
        /**************************************************
         * @return a list on all the teams of the data base
         **************************************************/
        public static Object cursorToObject(Cursor cursor) {
            boolean value = cursor .getInt(cursor.getColumnIndex(COLUMN_OBJECT_WORKING)) > 0;
            Object object = new Object(cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_CATEGORIE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_DESCRIPTION)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_OBJECT_TIMEFRAME)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_OBJECT_YEAR)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_BRAND)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_TECHNICALDETAILS)),
                    value,
                    cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_PICTURES))
            );

            return object;
        }
}
