package com.example.cerimuseum;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**************************************************
 * HACHETTE ALEXANDRE
 * 16/04/2020
 * Recover the ID of all the object present in the museum and set caracteristic of each object
 **************************************************/
public class IDRecover {

    private URL urlID;
    private ServiceUrl serviceUrl = new ServiceUrl();

    public void recover(ArrayList<Object> objects) {
        HttpURLConnection urlConnection = null;
        try {
            urlID = serviceUrl.buildSearchID();
            urlConnection = (java.net.HttpURLConnection) urlID.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JSONResponseHandlerID jsonResponseHandlerID = new JSONResponseHandlerID(objects);
            jsonResponseHandlerID.readJsonStream(in);
        } catch (
                MalformedURLException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        try {
            for(int i = 0; i < objects.size();i++) {
                urlID = serviceUrl.buildSearchObject(objects.get(i).getId());
                Log.i("urlTeam", String.valueOf(urlID));
                urlConnection = (HttpURLConnection) urlID.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                JSONResponseHandlerObject jsonResponseHandlerObject = new JSONResponseHandlerObject(objects.get(i));
                jsonResponseHandlerObject.readJsonStream(in);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }
}


