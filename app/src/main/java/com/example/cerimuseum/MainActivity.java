package com.example.cerimuseum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

/**************************************************
 * HACHETTE Alexandre
 * 15/04/2020
 * MainActivity
 **************************************************/
public class MainActivity extends AppCompatActivity implements MuseumAdapter.OnItemClick{

    private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();

    private RecyclerView listObject;
    private MuseumAdapter adapter;
    private Museum museum;
    private ArrayList<Object> objects;
    MuseumDbHelper museumDbHelper = new MuseumDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listObject = findViewById(R.id.listeObject);
        listObject.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listObject.setLayoutManager(layoutManager);

        museum = new Museum(this);
        objects = museum.getObjectList();
        adapter = new MuseumAdapter(objects,getApplicationContext(),this);
        listObject.setAdapter(adapter);
    }


    @Override
    public void OnItemClick(int position) {
        final String item = objects.get(position).getName();
        Intent myIntent = new Intent(MainActivity.this, ObjectActiviy.class);
        myIntent.putExtra("object", objects.get(position));
        MainActivity.this.startActivity(myIntent);

    }

    public void update() {
        for(int i = 0;i <objects.size();i++)
        {
            Log.d(TAG, "update: " + objects.get(i).getName());
        }
        for(int i = 0; i < objects.size(); i++) {
            museumDbHelper.updateObject(objects.get(i));
        }
        adapter.setListObject(objects);
        adapter.notifyDataSetChanged();
    }
}
