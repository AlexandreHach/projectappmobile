package com.example.cerimuseum;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
/**************************************************
 * HACHETTE ALEXANDRE
 * 16/04/2020
 * Adapter of the list view
 **************************************************/
class MuseumAdapter extends RecyclerView.Adapter<MuseumAdapter.Holder> {

    private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();

    private ArrayList<Object> listObject;
    private Context context;
    private OnItemClick mOnItemClick;

    MuseumAdapter(ArrayList<Object> listObject, Context context, OnItemClick onItemClick) {
        this.listObject = new ArrayList<>(listObject);
        this.mOnItemClick = onItemClick;
        this.context = context;
    }
    public static class Holder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        ImageView image;
        TextView nom;
        TextView categorie;
        OnItemClick onItemClick;

        Holder(View itemView, OnItemClick onItemClick) {
            super(itemView);
            image= itemView.findViewById(R.id.imageView);
            nom= itemView.findViewById(R.id.name);
            categorie= itemView.findViewById(R.id.categorie);
            this.onItemClick = onItemClick;
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            onItemClick.OnItemClick(getAdapterPosition());
        }

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.museum_object,parent,false);


        return new Holder(view, mOnItemClick);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Object object = listObject.get(position);
        String nomImage = object.getPictures();


        holder.nom.setText(object.getName() + " " + object.getBrand());
        holder.categorie.setText(object.getCategorie());

    }


    @Override
    public int getItemCount() {
        return listObject.size();
    }

    public interface OnItemClick
    {
        void OnItemClick(int position);
    }
    public void setListObject(ArrayList<Object> listObject)
    {
        this.listObject = listObject;
        notifyDataSetChanged();
    }



}
