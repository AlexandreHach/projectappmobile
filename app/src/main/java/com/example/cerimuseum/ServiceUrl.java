package com.example.cerimuseum;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;
/**************************************************
 * HACHETTE Alexandre
 * 15/04/2020
 * URL constructor class
 **************************************************/
public class ServiceUrl {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";


    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }



    /**************************************************
     * Build URL to get ids of objects in the museum.
     * URL example https://demo-lia.univ-avignon.fr/cerimuseum/ids
     **************************************************/
    private static final String SEARCH_TIMES = "ids";
    public static URL buildSearchID() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_TIMES);
        URL url = new URL(builder.build().toString());
        return url;
    }

    /**************************************************
     * Get characteristics of an object defined by the id passed.
     * https://demo-lia.univ-avignon.fr/cerimuseum/items/q2u
     **************************************************/
    private static final String TEAM_LAST_EVENT = "items";
    public static URL buildSearchObject(String idObject) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(TEAM_LAST_EVENT)
                .appendPath(idObject);
        URL url = new URL(builder.build().toString());
        return url;
    }
}
