package com.example.cerimuseum;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
/**************************************************
 * HACHETTE Alexandre
 * 15/04/2020
 * JSONResponser
 **************************************************/
public class JSONResponseHandlerObject {

        private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();


        private Object object;


        public JSONResponseHandlerObject(Object object) {
            this.object = object;
        }

        public void readJsonStream(InputStream response) throws IOException {
            JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
            try {
                readTeams(reader);
            } finally {
                reader.close();
            }
        }

        public void readTeams(JsonReader reader) throws IOException {
            reader.beginObject();
            while (reader.hasNext()) {
                String test = reader.nextName();
                Log.d(TAG, "readTeams: " + test);
                String name = test;
                if (name.equals("name")) {
                    object.setName(reader.nextString());
                } else if (name.equals("categories")){
                    reader.beginArray();
                    while(reader.hasNext())
                    {
                        object.setCategorie(reader.nextString());
                    }
                    reader.endArray();
                } else if (name.equals("description")) {
                    object.setDescription(reader.nextString());
                } else if (name.equals("timeFrame")) {
                    reader.beginArray();
                    while(reader.hasNext())
                    {
                        object.setTimeFrame(reader.nextInt());
                    }
                    reader.endArray();
                } else if (name.equals("year")) {
                    object.setYear(reader.nextInt());
                } else if (name.equals("brand")) {
                    object.setBrand(reader.nextString());
                } else if (name.equals("technicalDetails")) {
                    reader.beginArray();
                    while(reader.hasNext())
                    {
                        object.setTechnicalDetails(reader.nextString());
                    }
                    reader.endArray();
                } else if (name.equals("working")) {
                    object.setWorking(reader.nextBoolean());
                } else if (name.equals("pictures")) {
                    reader.beginObject();

                    while(reader.hasNext())
                    {
                        reader.nextName();
                        object.setPictures(reader.nextString());
                    }
                    reader.endObject();
                } else {
                    reader.skipValue();
                }

            }
            reader.endObject();
        }


        private void readArrayTeams(JsonReader reader) throws IOException {


        }



}
