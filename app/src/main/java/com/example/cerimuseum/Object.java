package com.example.cerimuseum;


import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**************************************************
 * HACHETTE Alexandre
 * 15/04/2020
 * Object class
 **************************************************/
public class Object implements Parcelable {

    public static final String TAG = JSONResponseHandlerObject.class.getSimpleName();

    private String name;
    private String id;
    private String categorie;
    private String description;
    private int timeFrame;
    private int year;
    private String brand;
    private String technicalDetails;
    private boolean working;
    private String pictures;

    public Object(String id, String name, String categorie, String description, int timeFrame, int year, String brand, String technicalDetails, boolean working,String pictures) {
        this.id = id;
        this.name = name;
        this.categorie = categorie;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.pictures = pictures;
    }

    public Object(String id) {
        this.id = id;

    }


    public String getId() {
        if (id != null)
            return id;
        else
            return "";

    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        if (name != null)
            return name;
        else
            return "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        if (description != null)
            return description;
        else
            return "";

    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        if (brand != null)
            return brand;
        else
            return "";
    }

    public void setBrand(String brand) {

            this.brand = brand;

    }
    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails += technicalDetails;
    }

    public String getCategorie() {
        if(categorie != null)
            return categorie;
        else
            return "";
    }

    public void setCategorie(String categorie) {
        Log.d(TAG, "setCategorie: "+ categorie);
        if(categorie != null)
            this.categorie += categorie;
    }

    public Integer getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(int timeFrame) {
        this.timeFrame += timeFrame;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public Object(Parcel in){
        this.id = in.readString();
        this.name = in.readString();
        this.categorie = in.readString();
        this.description = in.readString();
        this.timeFrame = in.readInt();
        this.year = in.readInt();
        this.brand = in.readString();
        this.technicalDetails = in.readString();
        this.working = (in.readInt() == 1);
        this.pictures = in.readString();
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.categorie);
        dest.writeString(this.description);
        dest.writeInt(this.timeFrame);
        dest.writeInt(this.year);
        dest.writeString(this.brand);
        dest.writeString(this.technicalDetails);
        dest.writeInt( this.working ? 1 : 0);
        dest.writeString( this.pictures);
    }


    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public java.lang.Object createFromParcel(Parcel source) {
            return new Object(source);
        }

        public Object[] newArray(int size) {
            return new Object[size];
        }
    };
}
