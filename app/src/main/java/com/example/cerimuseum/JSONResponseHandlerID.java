package com.example.cerimuseum;


import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**************************************************
 * HACHETTE ALEXANDRE
 * 15/04/2020
 * Build URL to get ids of objects in the museum.
 **************************************************/


public class JSONResponseHandlerID {

    private static final String TAG = JSONResponseHandlerID.class.getSimpleName();

    private ArrayList<Object> listObject;


    public JSONResponseHandlerID(ArrayList<Object> listObject) {
        this.listObject = listObject;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readID(reader);
        } finally {
            reader.close();
        }
    }

    public void readID(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            String test = reader.nextString();
            Log.d(TAG, "readArrayID: test= " + test);
            Object object = new Object(test);
            listObject.add(object);
        }
        reader.endArray();
    }
}


