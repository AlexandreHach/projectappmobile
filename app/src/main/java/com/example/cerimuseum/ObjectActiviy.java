package com.example.cerimuseum;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ObjectActiviy extends AppCompatActivity {

    TextView name;
    TextView year;
    TextView timeFrame;
    TextView categories;
    TextView description;
    TextView brand;
    TextView working;
    ImageView picture;
    TextView technicalDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_activiy);


        Object object = (Object) getIntent().getParcelableExtra("object");

        name = (TextView) findViewById(R.id.name);
        year = (TextView) findViewById(R.id.year);
        timeFrame = (TextView) findViewById(R.id.timeFrame);
        categories = (TextView) findViewById(R.id.categories);
        description = (TextView) findViewById(R.id.description);
        brand = (TextView) findViewById(R.id.brand);
        working = (TextView) findViewById(R.id.working);
        technicalDetails = (TextView) findViewById(R.id.technicalDetails);
        picture = (ImageView) findViewById(R.id.picture);
        updateView(object);


    }

    private void updateView(Object object) {

        name.setText(object.getName());
        year.setText(Integer.toString(object.getYear()));
        timeFrame.setText(Integer.toString(object.getTimeFrame()));
        categories.setText(object.getCategorie());
        description.setText(object.getDescription());
        brand.setText(object.getBrand());
        working.setText(Boolean.toString(object.isWorking()));
        technicalDetails.setText(object.getTechnicalDetails());

        //TODO : update imageBadge
    }


}
